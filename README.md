# json-server

> [JSON Server GitHub](https://github.com/typicode/json-server)

```sh
json-server --watch db.json
```

* http://localhost:3000/users
* http://localhost:3000/admin
* http://localhost:3000/compagnies
* http://localhost:3000/agencies